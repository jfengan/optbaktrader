from abc import ABC, abstractmethod
from Enum import Trading
from Instruments import Instrument
from datetime import datetime


class Signal(object):

    def __init__(self, instrument: Instrument, direction: Trading, qty: float):
        self.instrument = instrument
        self.direction = direction
        self.qty = qty


class Strategy(object):
    def __init__(self):
        pass

    @abstractmethod
    def generator(self, ts: datetime)->list:
        raise NotImplemented
