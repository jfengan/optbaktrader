from abc import ABC, abstractmethod


class Instrument(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def payoff(self):
        raise NotImplemented

    @abstractmethod
    def price(self):
        raise NotImplemented


class EuropeanOption(Instrument):

    def payoff(self):
        pass

    def price(self):
        pass


class Futures(Instrument):

    def payoff(self):
        pass

    def price(self):
        pass


class PerpetualSwap(Instrument):

    def payoff(self):
        pass

    def price(self):
        pass
