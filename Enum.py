from enum import Enum


class Coin(Enum):
    BTC = "BTC"
    ETH = "ETH"
    USD = "USD"
    USDT = "USDT"


class Type(Enum):
    Vanilla = "Vanilla"
    Inverse = "Inverse"


class InstrumentType(Enum):
    Option = "Option"
    Futures = "Futures"
    Perpetual = "Perpetual"


class Trading(Enum):
    BUY = "Buy"
    SELL = "Sell"
