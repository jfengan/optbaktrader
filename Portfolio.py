from OMS import OMS


class Portfolio:
    def __init__(self):
        self.oms = OMS()

    def enter_signal(self):
        pass

    def unwind_signal(self):
        pass

    def trade_at_fair(self):
        # call oms according to signal
        pass

    def evaluate(self):
        pass

    def hedge(self):
        # generate signal and call OMS
        pass
