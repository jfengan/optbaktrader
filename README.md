# optbaktrader

* BlackScholes -> implementation of classic blackscholes model for price and greeks computing

* Enum: enumerations for contract types, instrument types and trading activity types

* Instruments: base instrument class together with VanillaOption, perpetual swap and futures

* OMS: working as a trading gateway and booking system

* Order: class for order info

* Portfolio: core of the backTest, handling the signals, calling the oms when entering into signals, unwinding signals, clearing and hedging

* Strategy: a factory for generating signals